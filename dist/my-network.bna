PK
     0vPKq�b9         package.json{"engines":{"composer":"^0.14.0"},"name":"my-network","version":"0.1.11","description":"My Commodity Trading network","networkImage":"https://hyperledger.github.io/composer-sample-networks/packages/basic-sample-network/networkimage.svg","networkImageanimated":"https://hyperledger.github.io/composer-sample-networks/packages/basic-sample-network/networkimageanimated.svg","scripts":{"prepublish":"mkdirp ./dist && composer archive create --sourceType dir --sourceName . -a ./dist/my-network.bna","pretest":"npm run lint","lint":"eslint .","postlint":"npm run licchk","licchk":"license-check","postlicchk":"npm run doc","doc":"jsdoc --pedantic --recurse -c jsdoc.json","test-inner":"mocha -t 0 --recursive && cucumber-js","test-cover":"nyc npm run test-inner","test":"npm run test-inner"},"repository":{"type":"git","url":"https://github.com/hyperledger/composer-sample-networks.git"},"keywords":["sample","composer","composer-network"],"author":"Hyperledger Composer","license":"Apache-2.0","devDependencies":{"browserfs":"^1.2.0","chai":"^3.5.0","chai-as-promised":"^6.0.0","composer-admin":"^0.14.0-0","composer-cli":"^0.14.0-0","composer-client":"^0.14.0-0","composer-connector-embedded":"^0.14.0-0","composer-cucumber-steps":"^0.14.0-0","cucumber":"^2.2.0","eslint":"^3.6.1","istanbul":"^0.4.5","jsdoc":"^3.4.1","license-check":"^1.1.5","mkdirp":"^0.5.1","mocha":"^3.2.0","moment":"^2.17.1","nyc":"^11.0.2"},"license-check-config":{"src":["**/*.js","!./coverage/**/*","!./node_modules/**/*","!./out/**/*","!./scripts/**/*"],"path":"header.txt","blocking":true,"logInfo":false,"logError":true},"nyc":{"exclude":["coverage/**","features/**","out/**","test/**"],"reporter":["text-summary","html"],"all":true,"check-coverage":true,"statements":100,"branches":100,"functions":100,"lines":100}}PK
     0vPK�r���  �  	   README.md# My Commodity Trading network

> This is the "Hello World" of Hyperledger Composer samples, which demonstrates the core functionality of Hyperledger Composer by changing the value of an asset.

This business network defines:

**Participant**
`SampleParticipant`

**Asset**
`SampleAsset`

**Transaction**
`SampleTransaction`

**Event**
`SampleEvent`

SampleAssets are owned by a SampleParticipant, and the value property on a SampleAsset can be modified by submitting a SampleTransaction. The SampleTransaction emits a SampleEvent that notifies applications of the old and new values for each modified SampleAsset.

To test this Business Network Definition in the **Test** tab:

Create a `SampleParticipant` participant:

```
{
  "$class": "org.acme.sample.SampleParticipant",
  "participantId": "Toby",
  "firstName": "Tobias",
  "lastName": "Hunter"
}
```

Create a `SampleAsset` asset:

```
{
  "$class": "org.acme.sample.SampleAsset",
  "assetId": "assetId:1",
  "owner": "resource:org.acme.sample.SampleParticipant#Toby",
  "value": "original value"
}
```

Submit a `SampleTransaction` transaction:

```
{
  "$class": "org.acme.sample.SampleTransaction",
  "asset": "resource:org.acme.sample.SampleAsset#assetId:1",
  "newValue": "new value"
}
```

After submitting this transaction, you should now see the transaction in the Transaction Registry and that a `SampleEvent` has been emitted. As a result, the value of the `assetId:1` should now be `new value` in the Asset Registry.

Congratulations!
PK
     0vPKl�0�  �     permissions.acl/**
 * Access control rules for mynetwork
 */
rule Default {
    description: "Allow all participants full access"
    participant: "ANY"
    operation: ALL
    resource: "org.acme.mynetwork.*"
    action: ALLOW
}

rule SystemACL {
  description:  "System ACL to permit all access"
  participant: "org.hyperledger.composer.system.Participant"
  operation: ALL
  resource: "org.hyperledger.composer.system.**"
  action: ALLOW
}PK
     0vPK               models/PK
     0vPK���[�  �     models/sample.cto/**
 * My commodity trading network.
 */
namespace org.acme.mynetwork
asset Commodity identified by tradingSymbol {
  o String tradingSymbol
  o String description
  o String mainExchange
  o Double quantity
  --> Trader owner
}
participant Trader identified by tradeId {
  o String tradeId
  o String firstName
  o String lastName
}
transaction Trade {
  --> Commodity commodity
  --> Trader newOwner
}
PK
     0vPK               lib/PK
     0vPKE�~X�  �     lib/sample.js/*
* Track the trade of  commodity from ne trader to another
* @param {org.acme.mynetwork.Trade} trade - the trade to be processed
* "transaction"
 */

function tradeCommodity(trade) {
    trade.commodity.owner = trade.newOwner;
    return getAssetRegistry('org.acme.mynetwork.Commodity').then(function (assetRegistry) {
        return assetRegistry.update(trade.commodity);
    });
}PK 
     0vPKq�b9                       package.jsonPK 
     0vPK�r���  �  	             *  README.mdPK 
     0vPKl�0�  �               1  permissions.aclPK 
     0vPK                          models/PK 
     0vPK���[�  �               -  models/sample.ctoPK 
     0vPK                        �  lib/PK 
     0vPKE�~X�  �                 lib/sample.jsPK      �  �    